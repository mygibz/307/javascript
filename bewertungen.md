# Bewertungen Erklärvideos

**Gruppe Benjamin**
Inhalt:         6/10
Kreativität:    5/10
Umsetzung:    	5/10

**Gruppe Steffen**
Inhalt:    	9/10
Kreativität:    5/10
Umsetzung:    	7/10

**Gruppe Samuel**
Inhalt:    	7/10
Kreativität:    7/10
Umsetzung:    	8/10

**Gruppe Kai**
Inhalt:    	9/10
Kreativität:    4/10
Umsetzung:      9/10

**Total**
Kai:           22/30
Samuel:        22/30
Steffen:       21/30
Benjamin:      16/30
