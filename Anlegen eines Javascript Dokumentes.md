# Anlegen eines Javascript Dokumentes

```html
<!-- .html file -->
<script>
  alert('Hallo Welt!');
</script>
```

```html
<!-- .js file -->
<script src="script.js"></script>
<script defer src="script.js"></script>
<script async src="script.js"></script>
```

![](https://miro.medium.com/max/801/1*WdYxien-TBZTcNgVErT-cw.png)